(function () {

    'use strict';

    //UI containers
    const weatherContainer = document.getElementById('js-weather-app');
    const cityName = document.getElementById('js-city-name');
    const cityTemperature = document.getElementById('js-temperature');
    const cityIcon = document.getElementById('js-icon');
    
    const formContainer = document.getElementById('js-form');
    const formInputCity = document.getElementById('js-input-city');
    const formInputCountry = document.getElementById('js-input-country');    
    
    const errorMessageContainer = document.getElementById('js-error-container');
    
    //Classes
    const weather = new Weather;

    function init () {
        
        loadCity();

        bindEvents();

    };

    function loadCity() {

        //Always remove any anterior messages from the ui
        cleanErrorMessage();

        weather.getData()

        .then( data => {   
            
            updateUI(data);

        })
        .catch(err => {

            showErrorMessage(`Could not find the desired city. Please verify the fields and try again`);

        });

    };

    function bindEvents () {

        formContainer.addEventListener('submit', verifyForm);        

    };

    function verifyForm (evt) {

        evt.preventDefault();

        if (formInputCity.value !== '' && formInputCountry.value !== '') {            

            weather.updateCity(formInputCity.value, formInputCountry.value);

            loadCity();

        } else {

            showErrorMessage('please, fill the fields before sending');

        }                
            
    };

    function updateUI (data) {
        cityName.innerHTML = data.name;
        cityTemperature.innerHTML = data.main.temp;  
        cityIcon.setAttribute('src', `http://openweathermap.org/img/w/${data.weather[0].icon}.png`);  
        
    };

    function showErrorMessage(message) {        
        errorMessageContainer.innerHTML = message;
    };

    function cleanErrorMessage() {
        errorMessageContainer.innerHTML = '';
    };
    
    weatherContainer ? init() : console.log('Could not initiliaze weather app. Missing container with id = "js-weather-app"');    

})();
