class Weather {

    constructor(cityID) {

        this.APIID = 'c25ebab3b67783da214e850c24762456';
        this.city = 'Porto Alegre';
        this.country = 'br';

    };

    async getData () {

        let cityJSON = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.country}&APPID=${this.APIID}&units=metric`);
        
        if (cityJSON.status === 404) {

            return Promise.reject('error')
            
        } else {

            let cityData = await cityJSON.json();  

            return cityData;

        }

    }

    updateCity (city, country) {

        this.city = city;
        this.country = country;

    }
    
}