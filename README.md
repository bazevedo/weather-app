# weather-app challenge by Bruno Azevedo

## Points to improve
- Create a UI
- Create a class to manage the UI interactions
- Better error handling
- Store in localStorage the last city the user visited and show it when the user returns to the page
- Fix accessibility errors, like when the error message pops up, the browser need to know what to announce to screen readers
- Enable the user to use coordinates, zip codes or more than one city at the same time
- Create tests for the codebase